<?php

class XHPROF {
	protected static $instance;

	/* @var \XHProfRuns_Default */
	protected $runs;
	protected $enable;
	protected $run_ids;
	protected $run_types;

	public function __construct() {
		$_SERVER['HTTP_HOST'];
		$this->enable = false;
		$this->run_ids = [];
		$this->run_types = [];
		if( true ) {
			$this->enable = true;
			register_shutdown_function([$this, '__destruct']);
			include_once __DIR__ ."/utils/xhprof_lib.php";
			include_once __DIR__ ."/utils/xhprof_runs.php";
			$this->runs = new \XHProfRuns_Default();
		}
	}

	public function __destruct() {
		$this->finalize();
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function start() {
		if( $this->enable ) {
			// start profiling
			if( function_exists('my_ob_start') ) {
				my_ob_start('xhpro');
			} else {
				ob_start(null, 0, PHP_OUTPUT_HANDLER_STDFLAGS);
			}
			xhprof_enable();
		}
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function stop($type = '') {
		if( $this->enable ) {
			// stop profiler
			$xhprof_data        = xhprof_disable();
			if( empty($type) ) {
				$type           = "xhprof_".date('Y-m-d-H-i-s');
			} else {
				$type           = "xhprof_{$type}_".date('Y-m-d-H-i-s');
			}

			$run_id             = $this->runs->save_run( $xhprof_data, $type );
			$this->run_ids[]    = $run_id;
			$this->run_types[]  = $type;
		}
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function finalize() {
		static $called = false;
		if( $called ) {
			return $this;
		}
		$called = true;

		if( $this->enable ) {
			$this->stop();
			if( function_exists('my_ob_get_clean') ) {
				$xhpro = my_ob_get_clean('xhpro');
			} else {
				$xhpro = ob_get_clean();
			}
			if( !headers_sent() ) {
				foreach($this->run_ids as $idx => $run_id) {
					$url = sprintf('http://xhprof.local/index.php?run=%s&source=%s', $run_id, $this->run_types[$idx]);
					@header("xhpro_{$run_id}: {$url}");
				}
			}
			echo $xhpro;

			/*
			// save raw data for this profiler run using default
			// implementation of iXHProfRuns.
			// save the run under a namespace "xhprof_foo"
			echo "---------------\n".
			     "Assuming you have set up the http based UI for \n".
			     "XHProf at some address, you can view run at \n".
			     "<a href='http://".$_SERVER['HTTP_HOST']."/xhprof_html/index.php?run=$run_id&source=xhprof_foo'>click here</a>\n".
			     "---------------\n";
			*/
		}
		return $this;
	}

	public static function getInstance() {
		if( !isset(self::$instance) ) {
			self::$instance = new self();
		}
		return self::$instance;
	}
}