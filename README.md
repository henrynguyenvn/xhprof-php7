# xhprof for PHP7

Please do not use this in an production env.

生产环境勿用。
代码简直没法看。

git clone https://github.com/longxinH/xhprof.git

## Install

### Compile in Linux
```
$ /$PHP7/bin/phpize
$ ./configure --with-php-config=/$PHP7/bin/php-config
if you got error, pls run
Mac: brew install autoconf
CentOS: yum install autoconf
Ubuntu: apt-get install autoconf
Fedora: dnf install autoconf

$ make && make install
```
edit php.ini, add a new line:
```
extension=xhprof.so
```
make sure it works:
```
php7 -m |grep xhprof
```


## Recompile By Hyselab

### Original on github
git clone https://github.com/RustJason/xhprof.git
### Custom by Hyselab
git clone https://henrynguyenvn@bitbucket.org/henrynguyenvn/xhprof-php7.git
cd xhprof
git checkout php7
cd extension/
sudo apt install php7.0-dev
phpize
./configure
make
sudo make install


### Config PHP
`mkdir -p /var/tmp/xhprof`
`sudo chmod -R 777 /var/tmp/xhprof`
`nano /etc/php/7.0/mods-available/xhprof.ini`
And append code 

```
[xhprof]
extension=xhprof.so
xhprof.output_dir="/var/tmp/xhprof"
```
`sudo apt-get install -y graphviz`
`cd /etc/php/7.0/fpm/conf.d/`
`ln -s /etc/php/7.0/mods-available/xhprof.ini 20-xhprof.ini`
`sudo /etc/init.d/php7.0-fpm reload`

Run phpinfo() and check extension xhprof

### Config nginx for xhprof

```
server {
	listen 80;
	listen [::]:80;
	root <Your Path>/xhprof_html;
	index index.php index.html index.htm index.nginx-debian.html;

    server_name xhprof.local;

    access_log xhprof_access.log;
    error_log xhprof_error.log;

    location / {
		try_files $uri $uri/ =404;
	}

	location ~ \.php$ {
		include snippets/fastcgi-php.conf;
		# With php7.0-fpm:
		fastcgi_pass unix:/run/php/php7.0-fpm.sock;
    }
    location ~ /\.ht {
        deny all;
    }
}
```

### In your virtual host please add 
`fastcgi_param PHP_VALUE "auto_prepend_file=\"<Your path>/xhprof_lib/prepend.php\"\nauto_append_file=\"<Your path>/xhprof_lib/append.php\"";`


